/* eslint-disable react/forbid-prop-types */
import React, { useState, useRef, ElementRef } from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Rule, ThreadWithPosts } from 'knockout-schema';
import ErrorBoundary from '../../components/ErrorBoundary';
import LoggedInOnly from '../../components/LoggedInOnly';
import Post from '../../components/Post';
import ThreadSubheader from './components/ThreadSubheader';
import EditableTitle from './components/EditableTitle';
import {
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeTextColor,
  ThemeFontSizeHeadline,
} from '../../utils/ThemeNew';
import Modal from '../../components/Modals/Modal';
import ModalSelect from '../../components/Modals/ModalSelect';
import TagModal from './components/TagModal';
import ReportModal from '../../components/ReportModal';
import PostEditor from '../../components/PostEditor';
import { roleCheck, UserRoleRestricted } from '../../components/UserRoleRestricted';
import { StyledForumIcon } from '../../components/ForumIcon';
import { StyledPlaceholder } from '../../components/Placeholder';
import ViewerCount from './components/ViewerCount';
import ThreadViewerModal from './components/ThreadViewerModal';
import { isLoggedIn } from '../../utils/user';
import {
  loadHideRatingsFromStorageBoolean,
  loadRatingsXrayFromStorageBoolean,
} from '../../services/theme';
import { MODERATOR_ROLES } from '../../utils/roleCodes';
import { isDeletedPost } from '../../utils/deletedUser';
import DeletedPost from './components/DeletedPost';

const StyledThreadPage = styled.div`
  .thread-title {
    margin: calc(${ThemeVerticalPadding} / 2) ${ThemeHorizontalPadding};
    color: ${ThemeTextColor};
    font-size: ${ThemeFontSizeHeadline};
    line-height: 1.1;
    overflow-wrap: break-word;
    vertical-align: middle;

    ${StyledForumIcon} {
      margin-right: 5px;
      max-height: 35px;
      vertical-align: middle;
    }

    ${StyledPlaceholder} {
      margin-right: 7px;
    }
  }

  .thread-page-wrapper {
    display: block;
    margin-top: calc(${ThemeVerticalPadding} / 2);
    padding-top: 0;
    padding-bottom: ${ThemeVerticalPadding};
    padding-left: ${ThemeHorizontalPadding};
    padding-right: ${ThemeHorizontalPadding};
  }

  .thread-post-list {
    padding: 0;
    margin-top: calc(${ThemeVerticalPadding} / 2);
    margin-bottom: 0;
    margin-left: 0;
    margin-right: 0;
  }
`;

interface ThreadPageProps {
  thread: ThreadWithPosts;
  params: {
    id: string;
  };
  showingMoveModal: boolean;
  moveModalOptions: { text: string; value: number }[];
  currentPage: number;
  togglePinned: () => Promise<void>;
  toggleLocked: () => Promise<void>;
  toggleDeleted: () => Promise<void>;
  showMoveModal: () => Promise<void>;
  createAlert: () => Promise<void>;
  deleteAlert: () => Promise<void>;
  currentUserId: number | null;
  submitThreadMove: (threadId: number) => void;
  hideModal: () => void;
  refreshPosts: () => void;
  linkedPostId: number;
  rules: Rule[];
}

const ThreadPage = ({
  thread,
  params,
  showingMoveModal,
  moveModalOptions,
  currentPage,
  togglePinned,
  toggleLocked,
  toggleDeleted,
  showMoveModal,
  deleteAlert,
  createAlert,
  currentUserId,
  submitThreadMove,
  hideModal,
  refreshPosts,
  linkedPostId,
  rules = [],
}: ThreadPageProps) => {
  const [modalMove, setModalMove] = useState();
  const [reportModalOpen, setReportModalOpen] = useState(false);
  const [viewerModalOpen, setViewerModalOpen] = useState(false);
  const [reportPostId, setReportPostId] = useState(undefined);
  type PostEditorHandle = ElementRef<typeof PostEditor>;
  const editorRef = useRef<PostEditorHandle>();
  const [tagModal, showTagModal] = useState(false);
  const showNewPostEditor = roleCheck(MODERATOR_ROLES) || (!thread.locked && !thread.deleted);
  const ratingsXray = isLoggedIn() && loadRatingsXrayFromStorageBoolean();
  const showRatings = !loadHideRatingsFromStorageBoolean();
  const handleReportClick = (postId) => {
    setReportPostId(postId);
    setReportModalOpen(true);
  };

  return (
    <>
      <Modal
        iconUrl="/static/icons/rearrange.png"
        title="Move thread"
        cancelFn={hideModal}
        submitFn={() => submitThreadMove(modalMove)}
        isOpen={showingMoveModal}
      >
        <ModalSelect
          defaultText="Choose a subforum..."
          options={moveModalOptions}
          onChange={(e) => setModalMove(e.target.value)}
        />
      </Modal>
      <TagModal thread={thread} isOpen={tagModal} openFn={showTagModal} />
      <ReportModal
        postId={reportPostId}
        isOpen={reportModalOpen}
        close={() => setReportModalOpen(false)}
        rules={rules}
      />
      <StyledThreadPage>
        <Helmet defer={false}>
          <title>
            {thread.title ? `${thread.title} - Knockout!` : 'Knockout - Loading thread...'}
          </title>
        </Helmet>
        <h1 className="thread-title">
          <EditableTitle
            title={thread.title}
            byCurrentUser={currentUserId === thread.user?.id}
            threadId={thread.id}
            iconId={thread.iconId}
          />
        </h1>
        {thread.viewers && (
          <ViewerCount
            id={thread.id}
            viewers={thread.viewers}
            onClick={() => thread.viewers.users && setViewerModalOpen(true)}
          />
        )}
        {thread.viewers?.users?.length > 0 && (
          <UserRoleRestricted roleCodes={MODERATOR_ROLES}>
            <ThreadViewerModal
              viewers={thread.viewers?.users}
              modalOpen={viewerModalOpen}
              setModalOpen={setViewerModalOpen}
            />
          </UserRoleRestricted>
        )}
        <ThreadSubheader
          thread={thread}
          params={params}
          currentPage={currentPage}
          togglePinned={togglePinned}
          toggleDeleted={toggleDeleted}
          toggleLocked={toggleLocked}
          currentUserId={currentUserId}
          showMoveModal={showMoveModal}
          showTagModal={showTagModal}
          createAlert={createAlert}
          deleteAlert={deleteAlert}
        />
        <article className="thread-page-wrapper">
          <div className="thread-post-list">
            {thread.posts.map((post) => {
              if (isDeletedPost(post)) {
                return (
                  <DeletedPost
                    postThreadPostNumber={post.threadPostNumber}
                    postDate={post.createdAt}
                    postId={post.id}
                  />
                );
              }

              const isUnread =
                new Date(thread.subscriptionLastPostNumber) < new Date(post.threadPostNumber) ||
                new Date(thread.readThreadLastSeen) < new Date(post.createdAt);
              const isLinkedPost = linkedPostId && Number(post.id) === Number(linkedPostId);
              const byCurrentUser = currentUserId === post.user.id;

              return (
                <Post
                  key={`${post.id}-${post.updatedAt}`}
                  threadPage={currentPage}
                  threadId={thread.id}
                  threadLocked={thread.locked}
                  postId={post.id}
                  postBody={post.content}
                  postDate={post.createdAt}
                  postEdited={post.updatedAt}
                  postThreadPostNumber={post.threadPostNumber}
                  postPage={currentPage}
                  ratings={post.ratings}
                  user={post.user}
                  byCurrentUser={byCurrentUser}
                  bans={post.bans}
                  isUnread={isUnread}
                  countryName={post.countryName}
                  countryCode={post.countryCode}
                  isLinkedPost={isLinkedPost}
                  subforumId={thread.subforumId}
                  handleReplyClick={(text) => {
                    if (editorRef.current) editorRef.current.appendToContent(text);
                  }}
                  handleReportClick={handleReportClick}
                  postSubmitFn={refreshPosts}
                  canRate={!!currentUserId}
                  ratingsXray={ratingsXray}
                  showRatings={showRatings}
                />
              );
            })}
          </div>
          {showNewPostEditor && (
            <LoggedInOnly>
              <ErrorBoundary errorMessage="The editor has crashed. Your post was saved a few seconds ago (hopefully). Reload the page.">
                <div className="thread-new-post">
                  <PostEditor
                    threadId={Number(params.id)}
                    ref={editorRef}
                    postSubmitFn={refreshPosts}
                  />
                </div>
              </ErrorBoundary>
            </LoggedInOnly>
          )}
        </article>
        <ThreadSubheader
          thread={thread}
          params={params}
          currentPage={currentPage}
          togglePinned={togglePinned}
          toggleDeleted={toggleDeleted}
          toggleLocked={toggleLocked}
          currentUserId={currentUserId}
          showMoveModal={showMoveModal}
          showTagModal={showTagModal}
          createAlert={createAlert}
          deleteAlert={deleteAlert}
        />
      </StyledThreadPage>
    </>
  );
};

export default ThreadPage;
