import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import getEventsList from '../../services/events';

import EventsComponent from './components/EventsComponent';
import { isLoggedIn } from '../../components/LoggedInOnly';
import socketClient from '../../socketClient';

const EventsPage = () => {
  const [events, setEvents] = useState([]);
  const [eventsLoaded, setEventsLoaded] = useState(false);
  const [initialLoad, setInitialLoad] = useState(true);

  const getEvents = async () => {
    setEvents(await getEventsList());
    setEventsLoaded(true);
  };

  useEffect(() => {
    getEvents();
    socketClient.emit('events:join');
    socketClient.on('events:new', (event) => {
      setInitialLoad(false);
      setEvents((prevEvents) => [event, ...prevEvents].slice(0, 300));
    });

    return () => {
      socketClient.emit('events:leave');
      socketClient.off('events:new');
    };
  }, []);

  if (!isLoggedIn()) {
    window.location.replace('/login');
    return null;
  }

  return (
    <>
      <Helmet>
        <title>Ticker - Knockout!</title>
      </Helmet>
      <EventsComponent events={events} eventsLoaded={eventsLoaded} initialLoad={initialLoad} />
    </>
  );
};

export default EventsPage;
