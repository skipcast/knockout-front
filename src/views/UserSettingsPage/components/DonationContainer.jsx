import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { darken } from 'polished';
import { Panel, PanelTitle } from '../../../components/Panel';
import {
  ThemeHighlightStronger,
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';
import humanizeDuration from '../../../utils/humanizeDuration';

const formatDateFromIso = (isoDateString) => isoDateString.split('T').shift();

const DonationContainer = ({ handleDonationClick, donationUpgradeExpiresAt }) => {
  return (
    <Panel>
      <PanelTitle title="One Time Donation">One Time Donation</PanelTitle>

      <StyledDonationDescription>
        You can donate a custom amount as a single purchase. For every £3.50 you donate, your
        account will be upgraded to Knockout Gold for a month.
        <br />
      </StyledDonationDescription>

      <DonateButton background="#ad1a1a" onClick={handleDonationClick} type="button">
        <i className="fas fa-heart" />
        Donate Custom Amount
      </DonateButton>

      {donationUpgradeExpiresAt && (
        <StyledDonationDescription>
          Thank you for donating to Knockout! Your donation benefits will expire in
          {` ${humanizeDuration(
            new Date(),
            new Date(donationUpgradeExpiresAt)
          )} on ${formatDateFromIso(donationUpgradeExpiresAt)}.`}
        </StyledDonationDescription>
      )}
    </Panel>
  );
};

DonationContainer.propTypes = {
  handleDonationClick: PropTypes.func,
  donationUpgradeExpiresAt: PropTypes.string,
};

DonationContainer.defaultProps = {
  handleDonationClick: null,
  donationUpgradeExpiresAt: null,
};

const DonateButton = styled.button`
  display: block;
  position: relative;
  background: ${ThemeHighlightStronger};
  color: white;
  border: none;
  border-radius: 5px;
  margin: 9px auto;
  padding: 0 0 0 32px;
  font-size: 13px;
  text-align: center;
  width: 230px;
  height: 32px;
  line-height: 32px;
  cursor: pointer;
  overflow: hidden;

  &:active {
    opacity: 0.9;
  }

  &:disabled {
    pointer-events: none;
    background: ${(props) => darken(0.2, ThemeHighlightStronger(props))};
  }

  i {
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    width: 32px;
    line-height: 32px;
    text-align: center;
    background: rgba(0, 0, 0, 0.1);
  }

  span {
    padding: 0 10px;
  }
`;

const StyledDonationDescription = styled.div`
  display: flex;
  padding: ${ThemeHorizontalPadding} ${ThemeVerticalPadding};
  padding-top: 0;
  flex-direction: row;
  flex-wrap: wrap;
`;

export default DonationContainer;
