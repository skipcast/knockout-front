import React, { useState, useEffect } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';
import { Helmet } from 'react-helmet';
import { getAlerts, deleteAlertRequest } from '../../services/alerts';
import AlertsListThreadItem from './components/AlertsListThreadItem';
import BlankSlate from '../../components/BlankSlate';
import {
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeBackgroundLighter,
  ThemeFontSizeSmall,
  ThemeFontSizeHuge,
  ThemeTextColor,
} from '../../utils/ThemeNew';
import { isLoggedIn } from '../../components/LoggedInOnly';
import { pushNotification } from '../../utils/notification';
import updateSubscriptions from '../../utils/subscriptions';
import ThreadItemPlaceholder from '../../components/ThreadItemPlaceholder';
import Pagination from '../../components/Pagination';
import { loadDisplayNsfwFilterSettingFromStorageBoolean } from '../../utils/postOptionsStorage';
import socketClient from '../../socketClient';

const AlertsList = () => {
  const {
    params: { page = 1 },
  } = useRouteMatch();
  const [alerts, setAlerts] = useState([]);
  const [totalAlerts, setTotalAlerts] = useState(0);
  const [alertsLoaded, setAlertsLoaded] = useState(false);
  const dispatch = useDispatch();
  const nsfwFilterEnabled = loadDisplayNsfwFilterSettingFromStorageBoolean();

  useEffect(() => {
    const getAlertsList = async () => {
      const results = await getAlerts(nsfwFilterEnabled, page);
      setAlerts(results.alerts);
      setTotalAlerts(results.totalAlerts);
      setAlertsLoaded(true);
    };

    getAlertsList();
  }, [page]);

  useEffect(() => {
    updateSubscriptions(dispatch, alerts);
  }, [alerts, dispatch]);

  if (!isLoggedIn()) {
    window.location.replace('/login');
    return null;
  }

  const markUnreadAction = async (id) => {
    try {
      await deleteAlertRequest({ threadId: id });
      socketClient.emit('threadPosts:leave', id);
      const results = await getAlerts(nsfwFilterEnabled, page);
      setAlerts(results.alerts);
      setTotalAlerts(results.totalAlerts);
      pushNotification({ message: 'Successfully unsubscribed to thread.' });
    } catch (err) {
      console.error(err);
      pushNotification({ message: 'Could not unsubscribe to thread.', type: 'error' });
    }
  };

  let alertsContent;

  if (alertsLoaded && totalAlerts === 0) {
    alertsContent = <BlankSlate resourceNamePlural="subscriptions" />;
  } else if (!alertsLoaded) {
    alertsContent = Array(16)
      .fill(1)
      // eslint-disable-next-line react/no-array-index-key
      .map((item, index) => <ThreadItemPlaceholder key={`p${index}`} />);
  } else {
    alertsContent = alerts.map((alert) => {
      return (
        <AlertsListThreadItem
          key={alert.id}
          id={alert.id}
          createdAt={alert.thread.createdAt}
          deleted={alert.thread.deleted}
          iconId={alert.thread.iconId}
          lastPost={alert.thread.lastPost}
          locked={Boolean(alert.thread.locked)}
          postCount={alert.thread.postCount}
          unreadPostCount={alert.unreadPosts}
          title={alert.thread.title}
          firstUnreadId={alert.firstUnreadId}
          user={alert.thread.user}
          backgroundUrl={alert.thread.backgroundUrl}
          backgroundType={alert.thread.backgroundType}
          markUnreadAction={() => markUnreadAction(alert.id)}
        />
      );
    });
  }

  return (
    <StyledAlertsList>
      <Helmet>
        <title>Subscriptions - Knockout!</title>
      </Helmet>
      <h2>Subscriptions</h2>

      <nav className="subheader">
        <span className="back-and-title">
          <div className="left">
            <Link className="return-btn" to="/">
              <i className="fas fa-angle-left" />
              <span>Home</span>
            </Link>
          </div>
        </span>
        <Pagination
          showNext
          pagePath="/subscriptions/"
          totalPosts={totalAlerts}
          currentPage={page}
          pageSize={20}
        />
      </nav>
      {alertsContent}
      <nav className="subheader pagination">
        <Pagination
          showNext
          pagePath="/subscriptions/"
          totalPosts={totalAlerts}
          currentPage={page}
          pageSize={20}
        />
      </nav>
    </StyledAlertsList>
  );
};

export default AlertsList;

const StyledAlertsList = styled.div`
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};

  h2 {
    margin: 0 0 ${ThemeVerticalPadding} 0;
    font-size: ${ThemeFontSizeHuge};
  }

  nav.subheader {
    max-width: 100vw;
    padding-top: ${ThemeVerticalPadding};
    padding-bottom: ${ThemeVerticalPadding};
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    font-size: ${ThemeFontSizeSmall};
    color: ${ThemeTextColor};

    .return-btn {
      display: block;
      padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
      background: ${ThemeBackgroundLighter};

      span {
        margin-left: calc(${ThemeHorizontalPadding} / 2);
      }
    }

    .back-and-title {
      display: flex;
      height: 30px;
      overflow: hidden;
      align-items: stretch;
      margin-right: ${ThemeHorizontalPadding};
      text-decoration: none;
    }
  }

  .subheader.pagination {
    justify-content: flex-end;
  }
`;
