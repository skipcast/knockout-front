import dayjs from 'dayjs';

const shouldDoUkraineOverride = () => {
  // Special override to enable Ukraine themed logo and "FREE UKRAINE"/"STAND WITH UKRAINE"/etc. quotes
  return true;
};

export const aprilFools = () => {
  const currentDate = dayjs();
  return currentDate.month() === 3 && currentDate.date() === 1 && currentDate.year() === 2022;
};

export const getEventHeaderLogo = () => {
  // Ukraine override is prioritised over everything else
  if (shouldDoUkraineOverride()) {
    return {
      default: '/static/logo_ukraine_light.svg', // Default theme for logged out users is light mode, so use light mode variation by default
      light: '/static/logo_ukraine_light.svg',
      dark: '/static/logo_ukraine_dark.svg',
      classic: '/static/logo_ukraine_dark.svg',
    };
  }

  const currentDate = dayjs();

  if (currentDate.month() === 9) {
    return { default: '/static/logo_october.png' };
  }

  const christmasDate = dayjs(`12/25/${currentDate.year()}`);

  if (Math.abs(currentDate.diff(christmasDate, 'day')) <= 7) {
    return { default: '/static/logo_summer.svg' };
  }

  return null;
};

export const getEventColor = () => {
  const currentDate = dayjs();

  if (currentDate.month() === 9) {
    return '#ec7337';
  }

  const christmasDate = dayjs(`12/25/${currentDate.year()}`);

  if (Math.abs(currentDate.diff(christmasDate, 'day')) <= 7) {
    return '#ff3b4e';
  }

  return null;
};

export const getEventFont = () => {
  if (aprilFools()) {
    return "'Comic Sans MS', 'Comic Sans', sans-serif";
  }

  return null;
};

export const getEventText = () => {
  const currentDate = dayjs();

  if (currentDate.month() === 9) {
    return 'spookout';
  }

  if (currentDate.month() === 3 && currentDate.date() === 1 && currentDate.year() === 2021) {
    return 'amogus';
  }

  return 'knockout';
};

export const getEventQuotes = () => {
  // Ukraine override is prioritised over everything else
  if (shouldDoUkraineOverride()) {
    return [
      // Main Ukraine override quotes, based on the main hashtags that have been used around
      'Free Ukraine',
      'Stand with Ukraine',

      // These ones are just extra ones for more flavour, can disable whichever ones we don't want
      'Stop Putin',
      '"Russian warship, go fuck yourself"', // Wanted a Ukrainian version of this too, but couldn't find a reliable source for the translation
      'No to war',
      'Glory to Ukraine! Glory to the heroes!',
      'Слава Україні! Героям слава!,', // Ukrainian ver. of above
    ];
  }

  const currentDate = dayjs();

  const christmasDate = dayjs(`12/25/${currentDate.year()}`);

  if (Math.abs(currentDate.diff(christmasDate, 'day')) <= 7) {
    return [
      'Happy holidays!',
      "Summer's here!",
      "It's getting hot in here 🥵",
      'Ever wonder how Santa manages to wear those heavy clothes in December?',
      '☀️',
      '2020 edition',
      '🏄',
      '🎄',
      'This year, my gift to you is anime memes 🎁',
      'Brazilian Christmas Edition',
      "We didn't have enough money to put snow on the site this year, so we're going with a summer theme",
      'Cultural exchange edition',
      '"During summer temperatures can often reach 40 degrees Celsius (86 to 104 degrees Fahrenheit) in Rio de Janeiro and..."',
      "There... there's actually people in the other hemisphere?",
    ];
  }

  return null;
};
