import dayjs from 'dayjs';

const ratingsList = [
  'agree',
  'disagree',
  'funny',
  'friendly',
  'kawaii',
  'optimistic',
  'sad',
  'artistic',
  'informative',
  'idea',
  'zing',
  'winner',
  'glasses',
  'late',
  'dumb',
];

const currentDate = dayjs();

if (currentDate.month() === 3 && currentDate.date() === 1) {
  ratingsList.push('yeet');
}

export default ratingsList;
