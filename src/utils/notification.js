import { toast } from 'react-toastify';

export const pushNotification = ({ message, type = 'info' }) => {
  if (!message) {
    return;
  }

  toast[type](message, {
    position: 'bottom-right',
    autoClose: 4000,
    hideProgressBar: true,
  });
};

export const pushSmartNotification = (standardResponse) => {
  if (!standardResponse.error && !standardResponse.message) {
    return;
  }

  let type = 'info';

  if (standardResponse.error) {
    type = 'error';
  }

  pushNotification({ message: standardResponse.message || standardResponse.error, type });
};
