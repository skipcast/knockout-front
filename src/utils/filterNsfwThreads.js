export default (threads) => {
  const nsfwTagIndex = 1;

  return threads.filter((thread) => {
    if (!thread.tags || thread.tags.length === 0) {
      return thread;
    }

    return thread.tags.findIndex((tag) => {
      return tag[nsfwTagIndex] !== undefined;
    }) === -1
      ? thread
      : null;
  });
};
