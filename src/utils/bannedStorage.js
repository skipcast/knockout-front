export const loadBannedMessageFromStorage = () => {
  const banInformation = JSON.parse(localStorage.getItem('banInformation'));

  if (banInformation) {
    return banInformation;
  }
  return null;
};

export const saveBannedMessageToStorage = (data) => {
  const { banMessage, expiresAt, threadId, postContent } = data;
  const banInformation = JSON.stringify({ banMessage, expiresAt, threadId, postContent });

  localStorage.setItem('banInformation', banInformation);
};

export const clearBannedMessageFromStorage = () => {
  localStorage.removeItem('banInformation');
};
