const replaceBetween = (input, start, end, replacement) => {
  const existingContent = input.substr(start, end - start);

  return `${input.substring(0, start)}${replacement[0]}${existingContent}${
    replacement[1]
  }${input.substring(end)}`;
};

export default replaceBetween;
