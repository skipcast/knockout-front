export const NOTIFICATION_SET = 'NOTIFICATION_SET';
export const NOTIFICATION_READ = 'NOTIFICATION_READ';
export const NOTIFICATION_READ_ALL = 'NOTIFICATION_READ_ALL';

export function setNotifications(value) {
  return {
    type: NOTIFICATION_SET,
    value,
  };
}

export function readNotification(value) {
  return {
    type: NOTIFICATION_READ,
    value,
  };
}

export function readAllNotifications() {
  return {
    type: NOTIFICATION_READ_ALL,
  };
}
