import { io } from 'socket.io-client';
import config from '../config';

export default io(config.apiHost, {
  withCredentials: true,
  transports: ['websocket'],
});
