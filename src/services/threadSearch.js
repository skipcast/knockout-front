/* eslint-disable import/prefer-default-export */
import { authPost } from './common';

export const threadSearch = async (
  title,
  subforumId = null,
  tagIds = null,
  sortBy = 'relevance',
  sortOrder = 'desc',
  page = 1
) => {
  const res = await authPost({
    url: `/v2/threadSearch`,
    data: {
      title,
      subforum_id: subforumId,
      tag_ids: tagIds,
      sort_by: sortBy,
      sort_order: sortOrder,
      page,
    },
  });

  const { data } = res;

  return data;
};
