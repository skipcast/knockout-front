import icons from '../../config/icons';

const getShuffledArr = arr => {
  if (arr.length === 1) {return arr};
  const rand = Math.floor(Math.random() * arr.length);
  return [arr[rand], ...getShuffledArr(arr.filter((_, i) => i != rand))];
};

export function listIcons({ showAdminIcons = false }) {
  return getShuffledArr(icons)
    .filter(icon => (showAdminIcons ? true : icon.restricted !== true))
    .map(icon => ({ id: icon.id, url: icon.url, desc: icon.description }));
}

export function getIcon(id) {
  const icon = icons[id] || icons[0];
  return { id, url: icon.url, desc: icon.description };
}
