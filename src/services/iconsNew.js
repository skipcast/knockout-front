import icons from '../../config/icons';
import { roleCheck } from '../components/UserRoleRestricted';
import { MODERATOR_ROLES } from '../utils/roleCodes';

export function listIcons() {
  const showAdminIcons = roleCheck(MODERATOR_ROLES);

  return icons
    .filter((icon) => (showAdminIcons ? true : icon.restricted !== true))
    .map((icon) => ({
      id: icon.id,
      url: icon.url,
      desc: icon.description,
      category: icon.category,
    }));
}

export function getIcon(id) {
  const icon = icons[id] || icons[0];
  return { id, url: icon.url, desc: icon.description };
}
