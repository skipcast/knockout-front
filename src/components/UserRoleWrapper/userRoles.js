/* stylelint-disable property-no-vendor-prefix */
import { css } from 'styled-components';
import {
  ADMIN,
  BANNED_USER,
  BASIC_USER,
  GOLD_USER,
  PAID_GOLD_USER,
  LIMITED_USER,
  MODERATOR,
  MODERATOR_IN_TRAINING,
} from '../../utils/roleCodes';
import {
  ThemeBannedUserColor,
  ThemeGoldMemberColor,
  ThemeGoldMemberGlow,
  ThemeMemberColor,
  ThemeModeratorColor,
  ThemeModeratorInTrainingColor,
} from '../../utils/ThemeNew';

const goldUserRole = {
  name: 'Gold Member',
  color: '#fcbe20',
  extraStyle: css`
    color: transparent;
    filter: ${ThemeGoldMemberGlow};
    background: ${ThemeGoldMemberColor};
    background-clip: text;
    text-shadow: unset;
    -webkit-background-clip: text;
  `,
};

export default {
  [BANNED_USER]: {
    name: 'Banned User',
    color: ThemeBannedUserColor,
    extraStyle: null,
  },
  [LIMITED_USER]: {
    name: 'Member',
    color: ThemeMemberColor,
  },
  [BASIC_USER]: {
    name: 'Member',
    color: ThemeMemberColor,
  },
  [GOLD_USER]: goldUserRole,
  [PAID_GOLD_USER]: goldUserRole,
  [MODERATOR]: {
    name: 'Moderator',
    color: ThemeModeratorColor,
    extraStyle: null,
  },
  [MODERATOR_IN_TRAINING]: {
    name: 'Moderator in Training',
    color: ThemeModeratorInTrainingColor,
    extraStyle: null,
    title: 'Moderator in Training',
  },
  [ADMIN]: {
    name: 'Admin',
    color: '#fcbe20',
    extraStyle: css`
      color: transparent;
      filter: ${ThemeGoldMemberGlow};
      background: ${ThemeGoldMemberColor};
      background-clip: text;
      -webkit-background-clip: text;
    `,
  },
};
