/* eslint-disable react/display-name */
import React, { useState, forwardRef, useImperativeHandle, useEffect } from 'react';
import styled from 'styled-components';

import EditorBB from '../EditorBB';

import { handleNewPostSubmit, handleEditPostSubmit } from './helpers';
import {
  loadPostContentsFromStorage,
  savePostContentsToStorage,
} from '../../utils/postEditorAutosave';

interface PostEditorProps {
  threadId: number;
  initialContent?: string;
  type?: 'new' | 'edit' | 'code';
  postId?: number;
  postSubmitFn?: (content: string) => void;
}

interface PostEditorHandle {
  appendToContent: (text: string) => void;
}

const PostEditor = forwardRef<PostEditorHandle, PostEditorProps>(
  ({ threadId, initialContent, type, postId, postSubmitFn }: PostEditorProps, ref) => {
    const startingContent = initialContent || loadPostContentsFromStorage(threadId) || '';
    const [content, setContent] = useState(startingContent);
    const [submitting, setSubmitting] = useState(false);

    useEffect(() => {
      if (submitting) {
        if (type === 'new') {
          handleNewPostSubmit(content, threadId, postSubmitFn, setContent, setSubmitting);
        } else if (type === 'edit') {
          handleEditPostSubmit(content, threadId, postId, postSubmitFn, setSubmitting);
        }
      }
    }, [submitting]);

    const handleSetContent = (value) => {
      setContent(value);
      if (type === 'new') {
        savePostContentsToStorage(threadId, value);
      }
    };

    useImperativeHandle(ref, () => ({
      appendToContent(text) {
        setContent(content + text);
      },
    }));

    return (
      <StyledPostEditor>
        <EditorBB
          content={content}
          setContent={handleSetContent}
          handleSubmit={() => !submitting && setSubmitting(true)}
          editable={type !== 'code'}
        >
          <button
            type="submit"
            disabled={content.length === 0 || submitting}
            onClick={() => !submitting && setSubmitting(true)}
            title="Submit post (shortcut: ctrl+enter)"
          >
            <i className="fas fa-paper-plane" />
            &nbsp;Submit
          </button>
        </EditorBB>
      </StyledPostEditor>
    );
  }
);

const StyledPostEditor = styled.div`
  position: relative;
`;

export default PostEditor;

PostEditor.defaultProps = {
  initialContent: undefined,
  type: 'new',
  postId: undefined,
  postSubmitFn: () => {},
};
