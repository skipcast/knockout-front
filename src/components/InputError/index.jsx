import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { ThemeFontSizeSmall, ThemeBannedUserColor } from '../../utils/ThemeNew';

export const StyledInputError = styled.div`
  box-sizing: border-box;
  width: 100%;
  color: ${ThemeBannedUserColor};
  font-size: ${ThemeFontSizeSmall};
`;

const InputError = ({ error }) => {
  if (error) {
    return <StyledInputError>{error}</StyledInputError>;
  }

  return null;
};

InputError.propTypes = {
  error: PropTypes.string,
};

InputError.defaultProps = {
  error: undefined,
};

export default InputError;
