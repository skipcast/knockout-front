import { darken, lighten } from 'polished';
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { ThemeBackgroundDarker, ThemeHighlightWeaker } from '../../../utils/ThemeNew';

const FormSwitch = ({ checked, toggle }) => {
  return (
    <StyledSwtich role="switch" checked={checked} onClick={toggle}>
      <div className="button" />
    </StyledSwtich>
  );
};

FormSwitch.propTypes = {
  checked: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
};

const StyledSwtich = styled.button`
  display: flex;
  border: 2px solid transparent;
  background: ${(props) => {
    if (props.checked) return ThemeHighlightWeaker;
    if (props.theme.mode === 'light') return darken(0.2, ThemeBackgroundDarker(props));
    return lighten(0.2, ThemeBackgroundDarker(props));
  }};
  height: 24px;
  width: 40px;
  border-radius: 12px;
  outline: none;
  padding: 0;
  transition: background 0.3s;

  .button {
    border-radius: 50%;
    background: white;
    width: 20px;
    height: 20px;
    margin-left: ${(props) => (props.checked ? '16px' : 0)};
    transition: margin 0.3s;
    box-shadow: 0px 3px 1px -2px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14),
      0px 1px 5px 0px rgba(0, 0, 0, 0.12);
  }
`;

export default FormSwitch;
