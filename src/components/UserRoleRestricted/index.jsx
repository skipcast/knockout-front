import { useSelector } from 'react-redux';
import store from '../../state/configureStore';

export const roleCheck = (roleCodes = []) => {
  const state = store.getState();
  return roleCodes.includes(state.user?.role?.code);
};

export const UserRoleRestricted = ({ children, roleCodes = [] }) => {
  const userRoleCode = useSelector((state) => state.user?.role?.code);
  if (roleCodes.includes(userRoleCode)) {
    return children;
  }
  return null;
};

export default UserRoleRestricted;
