import React, { useContext, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { ThemeContext } from 'styled-components';

export const isReddit = (src) => {
  try {
    const url = new URL(src);
    if (url.hostname !== 'www.reddit.com') throw new Error();
    return true;
  } catch (error) {
    return null;
  }
};

export const useAsyncScript = (src, id = '', properties = []) => {
  useEffect(() => {
    let script;
    if (!id || !document.getElementById(id)) {
      const first = document.getElementsByTagName('script')[0];
      script = document.createElement('script');
      script.id = id;
      script.src = src;
      script.async = true;
      first.parentNode.insertBefore(script, first);
    }
    return () => {
      if (script) document.head.removeChild(script);
    };
  }, [src, ...properties]);
};

const RedditBB = ({ href, children }) => {
  const url = new URL(href || children.join(''));
  let { pathname } = url;
  if (pathname.endsWith('/')) pathname = pathname.slice(0, -1);

  const theme = useContext(ThemeContext);
  const container = useRef();

  const parseHeight = (event) => {
    try {
      if (typeof event.data === 'string') {
        const data = JSON.parse(event.data);
        if (data.type === 'resize.embed') {
          container.current.setAttribute('height', data.data + 10);
        }
      }
    } catch (error) {
      console.error('Message parse failed');
    }
  };

  useEffect(() => {
    window.addEventListener('message', parseHeight);
    return () => window.removeEventListener('message', parseHeight);
  }, []);

  return (
    <iframe
      id="reddit-embed"
      title="reddit embed"
      src={`https://www.redditmedia.com${pathname}/?ref_source=embed&amp;ref=share&amp;embed=true&amp;depth=1&amp;showmore=false&amp;showtitle=true${
        theme.mode !== 'light' ? '&amp;theme=dark' : ''
      }`}
      ref={container}
      sandbox="allow-scripts allow-same-origin allow-popups"
      style={{ border: 'none', maxWidth: '100%' }}
      scrolling="no"
      width="640"
      loading="lazy"
    />
  );
};

RedditBB.propTypes = {
  href: PropTypes.string.isRequired,
  children: PropTypes.string.isRequired,
};

export default RedditBB;
