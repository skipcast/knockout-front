import dayjs from 'dayjs';
import PropTypes from 'prop-types';
import React, { useState, useEffect, useContext, useRef } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import styled, { ThemeContext } from 'styled-components';
import config from '../../../config';
import { getUserProfile } from '../../services/user';
import {
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeFontSizeHuge,
  ThemeVerticalPadding,
} from '../../utils/ThemeNew';
import { TextButton } from '../Buttons';
import Tooltip from '../Tooltip';
import UserRoleWrapper from '../UserRoleWrapper';
import userRoles from '../UserRoleWrapper/userRoles';

const MiniProfile = ({ user, parent }) => {
  const TRANSITION = 400;
  const REVEAL_DELAY = 600;
  const [userProfile, setUserProfile] = useState({});
  const [loaded, setLoaded] = useState(false);
  const [isMobile, setIsMobile] = useState(false);
  const hoverTimer = useRef(null);

  const [visible, setVisible] = useState(false);
  const [rendered, setRendered] = useState(false);
  const hideTimer = useRef();
  const revealTimer = useRef();

  const currentUser = useSelector((state) => state.user);

  const showPopup = () => {
    const checkBeforeRender = () => {
      // wait for timeout to complete before setting rendered to true
      if (!hideTimer.current) {
        revealTimer.current = setTimeout(() => setRendered(true), REVEAL_DELAY);
      } else {
        setTimeout(checkBeforeRender, TRANSITION);
      }
    };
    checkBeforeRender();
  };

  const hidePopup = () => {
    setVisible(false);
  };

  const theme = useContext(ThemeContext);
  const defaultHeader =
    theme.mode === 'light' ? 'static/profile_header.png' : 'static/profile_header_dark.png';
  const headerImage = userProfile.header
    ? `${config.cdnHost}/image/${userProfile.header}`
    : defaultHeader;

  const hasAvatar =
    user.avatarUrl && user.avatarUrl.length !== 0 && !user.avatarUrl.includes('none.webp');
  const url = `${config.cdnHost}/image/${user.avatarUrl}`;

  const setHoverTimer = () => {
    clearTimeout(revealTimer.current);
    hoverTimer.current = setTimeout(() => {
      hidePopup();
    }, 300);
  };

  const clearHoverTimer = () => {
    clearTimeout(hoverTimer.current);
    showPopup();
  };

  useEffect(() => {
    const width =
      window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    setIsMobile(width <= 700);
  }, []);

  useEffect(() => {
    const setData = async () => {
      if (!loaded) {
        setUserProfile(await getUserProfile(user.id, true));
        setLoaded(true);
      }
      setVisible(true);
    };
    if (rendered) {
      setData();
    }
  }, [rendered]);

  useEffect(() => {
    if (!visible) {
      hideTimer.current = setTimeout(() => {
        setRendered(false);
        hideTimer.current = null;
      }, TRANSITION);
    }
  }, [visible]);

  useEffect(() => {
    const node = parent.current;

    if (node) {
      node.addEventListener('mouseover', () => {
        clearHoverTimer();
      });
      node.addEventListener('mouseout', () => {
        setHoverTimer();
      });
    }
    return () => {
      if (node) {
        node.removeEventListener('mouseover', () => {
          clearHoverTimer();
        });
        node.removeEventListener('mouseout', () => {
          setHoverTimer();
        });
      }
    };
  }, [parent]);

  return (
    !isMobile &&
    rendered && (
      <StyledMiniProfile
        onMouseOver={clearHoverTimer}
        onMouseOut={setHoverTimer}
        onFocus={clearHoverTimer}
        onBlur={setHoverTimer}
        hasAvatar={hasAvatar}
        visible={visible}
        transition={TRANSITION}
      >
        <div className="header">
          <img
            className="header-image"
            alt={`${user.username}'s profile header`}
            src={headerImage}
          />
        </div>
        <div className="profile-user">
          {hasAvatar && (
            <div className="avatar">
              <img className="avatar-image" src={url} alt={`${user.username}'s avatar`} />
            </div>
          )}
          <div className="user-heading">
            <Link to={`/user/${user.id}`} className="username-container">
              <UserRoleWrapper className="username" user={user}>
                {user.username}
              </UserRoleWrapper>
            </Link>
            {currentUser.loggedIn && currentUser.id !== user.id && (
              <Tooltip text="Message user">
                <TextButton className="message-button" as={Link} to={`/messages/new/${user.id}`}>
                  <i className="fas fa-envelope" />
                </TextButton>
              </Tooltip>
            )}
          </div>
          <div className="join-date">
            {`Member since ${dayjs(user.createdAt).format('MMMM YYYY')}`}
          </div>
          <UserRoleWrapper className="role" user={user}>
            <div className="role-icon-container">
              <i className="fas fa-square role-icon" />
            </div>
            {userRoles[user.role.code]?.name}
          </UserRoleWrapper>
          <div className="user-bio">{userProfile.bio}</div>
          <div className="user-summary">
            {user.posts > 0 && (
              <Link to={`/user/${user.id}/posts`}>
                <span className="summary-stat">{user.posts}</span>
                &nbsp;
                <span className="summary-item">{user.posts === 1 ? 'post' : 'posts'}</span>
              </Link>
            )}
            {user.threads > 0 && (
              <Link to={`/user/${user.id}/threads`}>
                <span className="summary-stat">{user.threads}</span>
                &nbsp;
                <span className="summary-item">{user.threads === 1 ? 'thread' : 'threads'}</span>
              </Link>
            )}
          </div>
        </div>
      </StyledMiniProfile>
    )
  );
};

MiniProfile.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
    username: PropTypes.string.isRequired,
    avatarUrl: PropTypes.string.isRequired,
    bio: PropTypes.string,
    role: PropTypes.shape({
      code: PropTypes.string,
    }),
    createdAt: PropTypes.string.isRequired,
    posts: PropTypes.number.isRequired,

    threads: PropTypes.number.isRequired,
  }),
  parent: PropTypes.shape({
    current: PropTypes.instanceOf(Element),
  }),
};

MiniProfile.defaultProps = {
  user: {
    bio: '',
    role: {
      code: '',
    },
  },
  parent: {
    current: null,
  },
};

const StyledMiniProfile = styled.div`
  position: absolute;
  top: 40px;
  left: 220px;
  width: 300px;
  z-index: 99;
  background: ${ThemeBackgroundDarker};
  box-shadow: rgba(0, 0, 0, 0.33) 0px 6px 14px;
  opacity: ${(props) => (props.visible ? 1 : 0)};
  transition: opacity ${(props) => props.transition}ms ease-in-out;

  .profile-user {
    padding: calc(${ThemeVerticalPadding} * 2);
    padding-top: 0;
  }

  .header {
    width: 100%;
    max-height: 100px;
    overflow: hidden;
    display: flex;
    align-items: center;
  }

  .header-image {
    display: block;
    position: relative;
    width: 120%;
    left: -10%;
    flex-shrink: 0;
  }

  .user-actions {
    display: flex;
    margin-top: 20px;
  }

  .avatar {
    background: ${ThemeBackgroundLighter};
    width: 65px;
    height: 65px;
    margin-top: -34px;
    position: relative;
  }

  .avatar-image {
    width: 100%;
    height: 100%;
    object-fit: contain;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
  }

  .user-heading {
    display: flex;
    align-items: center;
    margin-top: ${(props) => `calc(${ThemeVerticalPadding(props)} * ${props.hasAvatar ? 1 : 1.5})`};
    margin-bottom: 4px;
  }

  .username-container {
    flex-grow: 1;
  }

  .message-button {
    padding: 0;
    font-size: 24px;
  }

  .username {
    font-size: ${ThemeFontSizeHuge};
    font-weight: 600;
    display: block;
    line-height: normal;
  }

  .join-date {
    margin-top: 5px;
    opacity: 60%;
  }

  .role {
    margin-top: 10px;
    display: flex;
    align-items: center;
    overflow: initial;
  }

  .role-icon-container {
    display: flex;
    margin-right: 7px;
    margin-left: 2px;
  }

  .role-icon {
    transform: rotate(45deg);
    font-size: 12px;
  }

  .user-summary {
    margin-top: 20px;
  }

  .summary-stat {
    font-weight: bold;
  }

  .summary-item {
    margin-right: 10px;
  }

  .user-bio {
    margin-top: 20px;
    line-height: 1.2em;
  }
`;
export default MiniProfile;
