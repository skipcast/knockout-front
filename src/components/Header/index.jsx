import React, { useState, useEffect, useLayoutEffect, useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, Link } from 'react-router-dom';

import { Helmet } from 'react-helmet';
import { ThemeContext } from 'styled-components';
import loadable from '@loadable/component';
import { StyledHeader, HeaderLink } from './components/style';
import UserControls from './components/UserControls';
import BannedHeaderMessage from './components/BannedHeaderMessage';
import UserRoleRestricted from '../UserRoleRestricted';
import { scrollToTop } from '../../utils/pageScroll';
import LoggedInOnly from '../LoggedInOnly';

import { setNotifications } from '../../state/notifications';
import { loadBannedMessageFromStorage } from '../../utils/bannedStorage';
import { checkLoginStatus } from '../../utils/user';
import StyleableLogo from './components/StyleableLogo';
import SubscriptionsMenu from './components/SubscriptionsMenu';
import RepliesMenu from './components/RepliesMenu';
import updateSubscriptions from '../../utils/subscriptions';
import { getEventText } from '../../utils/eventDates';

import MessageOfTheDay from './components/MessageOfTheDay';
import { loadPunchyLabsFromStorageBoolean, getLogoPath } from '../../services/theme';
import { ThemeBackgroundLighter } from '../../utils/ThemeNew';
import { getNotifications } from '../../services/notifications';
import socketClient from '../../socketClient';
import { MODERATOR_ROLES } from '../../utils/roleCodes';
import SearchBar from './components/SearchBar';

const LogoQuotes = loadable(() => import('./components/LogoQuotes'));

const Header = () => {
  const [bannedInformation, setBannedInformation] = useState(undefined);
  const [openReports, setOpenReports] = useState(0);
  const [isOnTopOfPage, setisOnTopOfPage] = useState(true);

  const dispatch = useDispatch();

  const location = useLocation();

  const stickyHeader = useSelector((state) => state.style.stickyHeader);
  const isLoggedIn = useSelector((state) => state.user.loggedIn);

  const punchyLabsEnabled = loadPunchyLabsFromStorageBoolean();
  const theme = useContext(ThemeContext);

  const updateHeader = async ({ subscriptions, subscriptionIds, reports }) => {
    if (subscriptions && subscriptions[0]) {
      updateSubscriptions(dispatch, subscriptions);
      socketClient.emit('threadPosts:joinAll', subscriptionIds);
    }

    if (reports) {
      setOpenReports(reports || 0);
    }

    dispatch(setNotifications(await getNotifications()));
  };

  useEffect(() => {
    checkLoginStatus(location.pathname, (user) => updateHeader({ ...user }));

    if (isLoggedIn) {
      setBannedInformation(loadBannedMessageFromStorage());
    }
  }, []);

  useLayoutEffect(() => {
    const handleScroll = () => {
      setisOnTopOfPage(window.scrollY <= 5);
    };

    // Add scroll listener on mount
    window.addEventListener('scroll', handleScroll, { capture: true, passive: true });

    // Remove scroll listener on unmount
    return () => {
      window.removeEventListener('scroll', handleScroll, { capture: true, passive: true });
    };
  }, []);

  return (
    <>
      {bannedInformation && (
        <BannedHeaderMessage
          banMessage={bannedInformation.banMessage}
          expiresAt={bannedInformation.expiresAt}
          threadId={bannedInformation.threadId}
          postContent={bannedInformation.postContent}
        />
      )}
      <Helmet>
        <meta name="theme-color" content={ThemeBackgroundLighter({ theme })} />
      </Helmet>
      <StyledHeader
        id="knockout-header"
        stickyHeader={stickyHeader}
        $isOnTopOfPage={isOnTopOfPage}
        labs={punchyLabsEnabled}
      >
        <div id="header-content">
          <div className="branding">
            <Link to="/" className="brand">
              {getLogoPath() ? (
                <img src={getLogoPath()} className="header-logo" alt="Knockout logo" />
              ) : (
                <StyleableLogo className="header-logo" />
              )}
              <div className="title-container">
                <div className="title">{`${getEventText()}!`}</div>
                <LogoQuotes isOnTopOfPage={isOnTopOfPage} />
              </div>
            </Link>

            <div id="nav-items">
              <Link to="/rules" className={`link ${isLoggedIn && 'no-mobile'}`}>
                <i className="nav-icon fas fa-atlas" />
                <span className="nav-title">Rules</span>
              </Link>

              <Link className="link only-mobile" to="/threadsearch">
                <i className="nav-icon fas fa-search" />
                <span className="nav-title">Search</span>
              </Link>

              <LoggedInOnly>
                <Link to="/ticker" className="link" onClick={scrollToTop}>
                  <i className="nav-icon fas fa-stream" />
                  <span className="nav-title">Ticker</span>
                </Link>
              </LoggedInOnly>

              <SearchBar />

              <LoggedInOnly>
                <SubscriptionsMenu />

                <RepliesMenu />

                <UserRoleRestricted roleCodes={MODERATOR_ROLES}>
                  <HeaderLink
                    to="/moderate"
                    className="link"
                    onClick={() => {
                      setOpenReports(0);
                      scrollToTop();
                    }}
                  >
                    <i className="nav-icon fas fa-shield-alt" />
                    <span className="nav-title">Moderation</span>
                    {openReports > 0 && <div className="link-notification">{openReports}</div>}
                  </HeaderLink>
                </UserRoleRestricted>
              </LoggedInOnly>
            </div>
          </div>
          <UserControls />

          <div className="env-tag">{punchyLabsEnabled ? 'LABS' : 'BETA'}</div>
        </div>

        <MessageOfTheDay />

        <div className="backdrop-filter" />
      </StyledHeader>
    </>
  );
};

export default Header;
