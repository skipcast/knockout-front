import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import humanizeDuration from '../../../utils/humanizeDuration';
import { ThemeFontSizeHeadline } from '../../../utils/ThemeNew';

const BannedHeaderMessage = ({ banMessage, expiresAt, threadId, postContent }) => {
  const now = new Date();
  const expiresAtDate = Date.parse(expiresAt);
  const duration = humanizeDuration(now, expiresAtDate);
  return (
    <BanInfoWrapper>
      <BanInfoIcon
        src="https://img.icons8.com/color/50/000000/police-badge.png"
        alt="Banned!"
        title="Banned!"
      />
      <div>
        <div className="ban-info-header">You&apos;ve been Knocked Out&trade;!</div>
        {threadId && (
          <div className="ban-info-thread">
            <span>for this post: </span>
            <b>
              &quot;
              {postContent}
              &quot;&nbsp;
            </b>
            <span>
              in&nbsp;
              <BanInfoStrong to={`/thread/${threadId}`}>this thread</BanInfoStrong>
            </span>
          </div>
        )}
        <span>with the reason: </span>
        <b>
          &quot;
          {banMessage}
          &quot;
        </b>
        <div>
          {`Your ban will expire in `}
          <b title={expiresAt}>{duration}</b>
        </div>
        To appeal this ban, please visit the&nbsp;
        <BanInfoStrong to="/subforum/18">Whack Ass Crystal Prison.</BanInfoStrong>
      </div>
      <BanInfoIcon
        src="https://img.icons8.com/color/50/000000/police-badge.png"
        alt="Banned!"
        title="Banned!"
      />
    </BanInfoWrapper>
  );
};

BannedHeaderMessage.propTypes = {
  banMessage: PropTypes.string.isRequired,
  expiresAt: PropTypes.string.isRequired,
  threadId: PropTypes.string,
  postContent: PropTypes.string,
};

BannedHeaderMessage.defaultProps = {
  threadId: null,
  postContent: '',
};

export default BannedHeaderMessage;

export const BanInfoWrapper = styled.div`
  width: 100%;
  text-align: center;
  min-height: 50px;
  line-height: 25px;
  background: #dc4035;
  padding: 10px;
  margin-bottom: 10px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  box-sizing: border-box;
  color: white;

  .ban-info-header {
    font-size: ${ThemeFontSizeHeadline};
    font-weight: bold;
    margin-bottom: 15px;
  }

  .post-preview {
    font-weight: bold;
  }
`;

export const BanInfoIcon = styled.img`
  width: 50px;
  height: 50px;
  margin-right: 15px;
`;

export const BanInfoText = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  width: 100%;
  min-height: 40px;
`;

export const BanInfoStrong = styled(Link)`
  font-weight: bold;
  text-decoration: underline;
`;
