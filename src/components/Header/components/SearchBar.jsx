import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';
import { pushSmartNotification } from '../../../utils/notification';
import {
  ThemeBackgroundDarker,
  ThemeFontFamily,
  ThemeFontSizeMedium,
  ThemeTextColor,
} from '../../../utils/ThemeNew';

const SearchBar = () => {
  const [query, setQuery] = useState('');
  const history = useHistory();
  const handleSubmit = (e) => {
    if (query.length > 0) {
      if (query.length < 5) {
        pushSmartNotification({ error: 'Search query must be at least 5 characters long.' });
      } else {
        history.push(`/threadsearch/${query}`);
      }
    }
    e.preventDefault();
  };

  return (
    <StyledSearchBar className="no-mobile">
      <form className="search-form" onSubmit={handleSubmit}>
        <i className="search-icon nav-icon fas fa-search" />
        <input
          type="text"
          className="search-input"
          placeholder="Search threads"
          value={query}
          onChange={(e) => setQuery(e.target.value)}
        />
      </form>
    </StyledSearchBar>
  );
};

export default SearchBar;

const StyledSearchBar = styled.div`
  order: 4;
  flex-grow: 1;
  flex-shrink: 1;
  height: 67%;
  border: 1px solid ${ThemeBackgroundDarker};
  border-radius: 3px;
  padding-left: 15px;
  margin-right: 30px;

  .search-icon {
    opacity: 0.75;
    font-size: 0.875rem;
  }

  .search-form {
    display: flex;
    align-items: center;
    height: 100%;
  }

  .search-input {
    height: 100%;
    padding-left: 6px;
    width: 100%;
    outline: none;
    border: none;
    background: none;
    font-family: ${ThemeFontFamily};
    font-size: ${ThemeFontSizeMedium};
    color: ${ThemeTextColor};
    padding-bottom: 2px;
  }
`;
