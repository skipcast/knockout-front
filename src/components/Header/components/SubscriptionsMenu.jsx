import React, { useRef, useEffect, useState } from 'react';
import styled from 'styled-components';
import { HashLink as Link } from 'react-router-hash-link';
import { useDispatch, useSelector } from 'react-redux';
import { getIcon } from '../../../services/icons';
import useDropdownMenu, {
  DropdownMenuButton,
  DropdownMenuEmpty,
  DropdownMenuHeader,
  DropdownMenuItem,
  DropdownMenuOpened,
} from './DropdownMenu';
import {
  ThemeFontSizeMedium,
  ThemeFontSizeSmall,
  ThemeHighlightWeaker,
  ThemeHorizontalPadding,
} from '../../../utils/ThemeNew';
import socketClient from '../../../socketClient';
import { addSubscriptionThread, updateSubscriptionThread } from '../../../state/subscriptions';
import subscriptionsBroadcastChannel from '../../../subscriptionsBroadcastChannel';

const SubscriptionsMenu = () => {
  const menuRef = useRef();
  const buttonRef = useRef();
  const actionList = useRef([]);
  const [open, setOpen] = useDropdownMenu(menuRef, buttonRef);
  const [threads, setThreads] = useState([]);
  const subscriptions = useSelector((state) => state.subscriptions);
  const currentUser = useSelector((state) => state.user);
  const dispatch = useDispatch();

  useEffect(() => {
    const threadList = Object.keys(subscriptions.threads).map((item) => ({
      ...subscriptions.threads[item],
      id: item,
    }));
    setThreads(
      threadList.sort((threadA, threadB) => {
        if (threadA.count > threadB.count) return -1;
        if (threadA.count < threadB.count) return 1;
        return 0;
      })
    );
  }, [subscriptions.threads]);

  useEffect(() => {
    socketClient.on('post:new', (value) => {
      const threadInfo = {
        id: value.thread.id,
        title: value.thread.title,
        iconId: value.thread.iconId,
        post: value.id,
        locked: value.thread.locked,
      };
      if (currentUser.id !== value.user) {
        if (subscriptions.threads[value.thread.id]) {
          threadInfo.count =
            value.threadPostNumber - subscriptions.threads[value.thread.id].postNum;
          dispatch(updateSubscriptionThread(threadInfo, false));
        } else {
          threadInfo.count = 1;
          threadInfo.postNum = value.threadPostNumber - 1;
          threadInfo.page = value.page;
          dispatch(addSubscriptionThread(threadInfo, false));
        }
      }
    });

    return () => {
      socketClient.off('post:new');
    };
  }, [dispatch, currentUser.id, subscriptions.threads]);

  useEffect(() => {
    subscriptionsBroadcastChannel.onmessage = (event) => {
      if (open) {
        actionList.current.push(event.data);
      } else {
        dispatch(event.data);
      }
    };
    return () => {
      subscriptionsBroadcastChannel.onmessage = null;
    };
  }, [dispatch, open]);

  useEffect(() => {
    if (!open && actionList.current.length > 0) {
      actionList.current.forEach((action) => {
        dispatch(action);
      });
      actionList.current = [];
    }
  }, [dispatch, open]);

  const checkRefresh = (thread, event) => {
    const link = `/thread/${thread.id}/${thread.page}`;
    if (link === window.location.pathname) {
      event.preventDefault();
      window.location.reload();
    }
  };

  return (
    <SubscriptionsMenuButton
      className="subscriptions-menu"
      onClick={(e) => {
        if (!e.ctrlKey) setOpen((value) => !value);
      }}
      onKeyDown={(e) => {
        if (e.key === 'Enter') {
          setOpen((value) => !value);
        }
      }}
      tabIndex="0"
      ref={buttonRef}
    >
      <div className="dropdown-menu-button-inner" title="Subscriptions">
        <i className="fas fa-newspaper menu-icon" />
        {threads.length > 0 && (
          <div className="link-notification">{threads.length < 10 ? threads.length : '9+'}</div>
        )}
      </div>
      {open && (
        <DropdownMenuOpened className="subscriptions-menu-dropdown" ref={menuRef}>
          <DropdownMenuHeader>
            <h1 className="dropdown-menu-header-text">Subscriptions</h1>
          </DropdownMenuHeader>
          <div className="dropdown-menu-body">
            {threads.length > 0 ? (
              threads.map((thread) => {
                const icon = getIcon(thread.iconId);
                return (
                  <DropdownThreadItem
                    to={`/thread/${thread.id}/${thread.page}${
                      thread.postId ? `#post-${thread.postId}` : ``
                    }`}
                    onClick={(e) => checkRefresh(thread, e)}
                    key={thread.id}
                  >
                    <DropdownMenuItem>
                      <img className="dropdown-thread-icon" src={icon.url} alt={icon.description} />
                      <div className="dropdown-thread-title" title={thread.title}>
                        {thread.locked && (
                          <>
                            <i className="locked fas fa-lock" title="Locked" />
                            &nbsp;
                          </>
                        )}
                        {thread.title}
                      </div>
                      <div className="dropdown-thread-count">{thread.count}</div>
                    </DropdownMenuItem>
                  </DropdownThreadItem>
                );
              })
            ) : (
              <DropdownMenuEmpty>
                <i className="fas fa-comments dropdown-menu-empty-icon" />
                <h2 className="dropdown-menu-empty-header">No new posts</h2>
                <div className="dropdown-menu-empty-desc">
                  Subscribe to a thread to be notified when it has new posts.
                </div>
              </DropdownMenuEmpty>
            )}
          </div>
          <Link
            to="/subscriptions"
            className="dropdown-menu-footer"
            onKeyDown={(e) => {
              if (e.key === 'Enter') {
                window.location.href = '/subscriptions';
              }
            }}
          >
            View all
          </Link>
        </DropdownMenuOpened>
      )}
    </SubscriptionsMenuButton>
  );
};

export const SubscriptionsMenuButton = styled(DropdownMenuButton)`
  @media (min-width: 900px) {
    order: 5;
    height: 100%;
    margin-right: 10px;
    padding: 0px 10px;
  }

  @media (max-width: 900px) {
    padding: 10px;
    .menu-icon {
      font-size: ${ThemeFontSizeMedium};
    }
  }
`;

const DropdownThreadItem = styled(Link)`
  opacity: 0.75;

  .dropdown-thread-icon {
    width: 35px;
    margin-right: ${ThemeHorizontalPadding};
  }

  .dropdown-thread-title {
    font-size: ${ThemeFontSizeMedium};
    font-weight: 600;
    margin-right: ${ThemeHorizontalPadding};
    flex: 1;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    height: calc(${ThemeFontSizeMedium} * 1.1);
  }

  .dropdown-thread-count {
    border-radius: 50%;
    background: ${ThemeHighlightWeaker};
    padding: 6px;
    font-size: ${ThemeFontSizeSmall};
    font-weight: bold;
  }

  .locked {
    color: #ffcb00;
  }
`;

export default SubscriptionsMenu;
