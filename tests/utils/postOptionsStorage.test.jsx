/* eslint-disable no-underscore-dangle */
import * as PostOptionsStorage from '../../src/utils/postOptionsStorage';
import '@testing-library/jest-dom/extend-expect';

describe('Post Options Storage', () => {
  describe('as a logged in user', () => {
    beforeEach(() => {
      localStorage.clear();
    });

    it('loads with a key that has no value without a firstCheckKey', () => {
      const key = 'testKey';
      const defaultValue = 'testValue1';
      const result = PostOptionsStorage.loadValue(key, defaultValue);
      expect(result).toBe(defaultValue);
    });

    it('loads with a key that has a value without a firstCheckKey', () => {
      const key = 'testKey';
      const defaultValue = 'testValue1';
      PostOptionsStorage.loadValue(key, defaultValue);
      expect(localStorage.getItem).toHaveBeenCalledWith(key);
    });

    it('loads a value with a firstCheckKey', () => {
      const key = 'testKey';
      const defaultValue = 'testValue1';
      const firstCheckKey = 'firstTestKey';
      const firstAutoValue = 'firstAutoValue';
      PostOptionsStorage.loadValue(key, defaultValue, firstCheckKey, firstAutoValue);
      expect(localStorage.getItem).toHaveBeenCalledWith(firstCheckKey);
    });

    it('saves a value', () => {
      const key = 'testKey';
      const value = 'testValue';
      PostOptionsStorage.saveValue(key, value);
      expect(localStorage.setItem).toHaveBeenCalledWith(key, JSON.stringify(value));
    });
  });
});
