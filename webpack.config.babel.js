const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');
const appConfig = require('./appConfig');

/**
 * @type {import('webpack').Configuration}
 */
module.exports = {
  resolve: {
    extensions: ['.jsx', '.js', '.tsx', '.ts'],
    fallback: { "tty": false }
  },
  entry: './src/App.jsx',
  devtool: 'source-map',
  output: {
    filename: 'static/js/[name].[contenthash:4].js',
    chunkFilename: 'static/js/[chunkhash:4][name].[contenthash:4].js',
    publicPath: '/',
    path: path.resolve(__dirname, './dist/client'),
  },
  devServer: {
    historyApiFallback: true,
  },
  optimization: {
    minimizer: [
      new TerserPlugin({
        test: /\.(js|jsx)$/,
        parallel: true,
        terserOptions: {
          mangle: true,
          output: { comments: false },
          sourceMap: false,
        },
        extractComments: false,
      }),
    ],
    splitChunks: {
      chunks: 'all',
    },
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: { minimize: true },
          },
        ],
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: '!!raw-loader!./static/template.ejs',
      filename: './index.ejs',
    }),
    new HtmlWebpackPlugin({
      template: './static/template.ejs',
      filename: './index.html',
      templateParameters: {
        title: 'Knockout Forums',
        description: 'A welcoming gaming and lifestyle community!',
        type: 'website',
        url: 'https://knockout.chat/',
        image: 'https://knockout.chat/static/logo.png',
        date: new Date().toISOString(),
        schema: '',
        author: '',
        twitterCard: false,
        icon: appConfig.favicon,
      },
    }),
    new WorkboxPlugin.GenerateSW({
      clientsClaim: true,
      skipWaiting: true,
    }),
    new webpack.EnvironmentPlugin({
      NODE_ENV: 'development',
    }),
    new webpack.DefinePlugin({
      'process.appConfig': JSON.stringify(appConfig),
    }),
  ],
};
